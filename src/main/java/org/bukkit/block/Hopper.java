package org.bukkit.block;

import com.destroystokyo.paper.loottable.LootableInventory; // Paper
import org.bukkit.inventory.InventoryHolder;

/**
 * Represents a hopper.
 */
public interface Hopper extends BlockState, InventoryHolder, LootableInventory { // Paper

}
